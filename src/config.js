'use strict';

const config = {
  SettingsProvider: {
    lambdaPath: '../../..',
    settingsDir: 'settings',
    excludeDirs: ['.git', 'node_modules']
  }
};

module.exports = config;
