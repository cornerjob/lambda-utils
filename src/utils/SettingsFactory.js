'use strict';

const logger = require('simple-logger').getLogger('SettingsFactory');

const SettingsFactory = function () {};

SettingsFactory.getSettings = function (path) {
  let settings, environment;

  try {
    environment = process.env.ENVIRONMENT.toLowerCase();
  }
  catch (err) {
    return logger.error('Cannot find settings.  Please make sure envar ENVIRONMENT is set correctly');
  }

  switch (environment) {
    case 'dev':
      settings = require(`${path}/SettingsDev.js`);
      break;

    case 'pre':
      settings = require(`${path}/SettingsPre.js`);
      break;

    case 'pro':
      settings = require(`${path}/SettingsPro.js`);
      break;

    default:
      logger.error('Cannot find settings.  Please make sure envar ENVIRONMENT is set correctly');
  }

  return settings;
};

module.exports = SettingsFactory;
