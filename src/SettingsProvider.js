'use strict';

const finder = require('fs-finder');
const SettingsFactory = require('./utils/SettingsFactory.js');
const { lambdaPath, settingsDir, excludeDirs } = require('./config.js').SettingsProvider;
const logger = require('simple-logger').getLogger('SettingsProvider');
const env = process.env.ENVIRONMENT;

const SettingsProvider = function () {};

SettingsProvider.getSettings = function () {
  try {
    const dirs = getLambdaDirs();
    const settingsPath = getSettingsPath(dirs);

    if (settingsPath) {
      return getSettings(settingsPath);
    }

    logger.error('Cannot find settings directory.  Please make sure the directory exists');
  }
  catch (err) {
    if (err.code === 'MODULE_NOT_FOUND') {
      return logger.error(`Cannot find ${env} settings file.  Please make sure the file exists`);
    }

    logger.error(err);
  }
};


const getLambdaDirs = function () {
  return finder
    .from(`${__dirname}/${lambdaPath}`)
    .exclude(excludeDirs)
    .findDirectories();
};

const getSettingsPath = function (dirs) {
  for (let i in dirs) {
    if (dirs[i].split('/').pop() === settingsDir) {
      return dirs[i];
    }
  }
};

const getSettings = function (settingsPath) {
  const settings = SettingsFactory.getSettings(settingsPath);

  if (settings) {
    logger.info('Lambda settings:\n', JSON.stringify(settings, null, '\t'));
    return settings;
  }
};

module.exports = SettingsProvider;
