'use strict';

const logger = require('simple-logger').getLogger('EventParser');

const EventParser = function () {};

EventParser.parseDataFromRecord = function (record) {
  let eventData = {};

  try {
    const payload = new Buffer(record.kinesis.data, 'base64').toString('utf8');
    eventData = JSON.parse(payload);
  }
  catch (err) {
    logger.error(err);
  }

  return eventData;
};

module.exports = EventParser;
