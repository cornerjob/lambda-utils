'use strict';

const EventParser = require('./EventParser.js');
const logger = require('simple-logger').getLogger('RecordHandler');

const RecordHandler = function () {};

RecordHandler.promiseAllRecords = function (records, promiseCallback) {
  const promiseRecords = [];

  records.forEach(record => {
    promiseRecords.push(promiseRecord(record, promiseCallback));
  });

  return Promise.all(promiseRecords);
};

const promiseRecord = (record, promiseCallback) => {
  const data = EventParser.parseDataFromRecord(record);

  return promiseCallback(data)
    .catch(err => {
      return promiseNextRecord(err);
    });
};

const promiseNextRecord = message => {
  if (message) {
    logger.error(message);
  }

  return Promise.resolve();
};

module.exports = RecordHandler;
