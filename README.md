# lambda-utils #

Utility library for sharing code used across lambda functions.  The idea is you find yourself writing the same thing again and again in an AWS lambda, put it here.

## Install ##
```
#!bash
npm install --save bitbucket:cornerjob/lambda-utils
```

## How to use ##
#
#### Auto-config

By default, the lib will check if the envar **ENVIRONMENT** is set to either '*dev*' (localhost), '*pre*' (pre-production), '*pro*' (production).  Reading the envar is case-insensitive so you can set with capitals if you like.

It will then search for the appropriate *SettingsDev*, *SettingsPre*, or *SettingsPro* js file inside of a */settings* dir and use it as the lambda function configuration.
#
```
#!javascript
const { SettingsProvider } = require('lambda-utils');

exports.handler = function (event, context, callback) {
    const settings = SettingsProvider.getSettings();
};
```
#
#### Kinesis record handling

You can now easily process each Kinesis record with the help of the **RecordHandler**.  Calling ***promiseAllRecords()*** allows you to get straight to the business logic of your lambda function.  You just need to pass it the event records to process and a callback containing the business logic you would like to be executed for each event record.  By default the callback accepts a single parameter that provides access the payload of the record already base64 decoded and in JSON format.

As the method name suggests, ***promiseAllRecords()*** uses promises to process each record asynchronously.  Therefore in order to finish processing the current record and move on to the next, you need to return a promise that either resolves or rejects.  It does not matter whether you resolve or reject the promise as the library will always catch any errors and then resolve in order to continue processing the remaining records.
#

```
#!javascript
const { RecordHandler } = require('lambda-utils');

exports.handler = function ({ Records }, context, callback) {
  RecordHandler
    .promiseAllRecords(Records, data => {
      return new Promise(resolve => {
        console.log('payload data:', data);
        resolve();
      });
    })
    .then(results => {
      console.log('done');
      callback();
    });
  };
}
```
OR
```
#!javascript
const { RecordHandler } = require('lambda-utils');

exports.handler = function ({ Records }, context, callback) {
  RecordHandler
    .promiseAllRecords(records, data => {
      console.log('data:', data);
      return Promise.resolve();
    })
    .then(results => {
      console.log('done');
      callback();
    });
}
```
#
#### Event parsing

Event parsing comes free with the RecordHandler, but if you decide you want to parse the payload manually, you can use the EventParser to parse it from a kinesis record.
#

```
#!javascript
const { EventParser } = require('lambda-utils');

exports.handler = function (event, context, callback) {
    event.Records.forEach(record => {
      const data = EventParser.parseDataFromRecord(record);
    });
};
```