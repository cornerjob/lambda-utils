const LambdaUtils = {
  SettingsProvider: require('./src/SettingsProvider.js'),
  EventParser: require('./src/EventParser.js'),
  RecordHandler: require('./src/RecordHandler.js')
};

module.exports = LambdaUtils;
